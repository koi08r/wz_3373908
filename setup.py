from setuptools import setup


# WINEPREFIX=~/.wine-py2 wine 'c:\Python27\python.exe' -m pip install -r requirements.txt
# WINEPREFIX=~/.wine-py2 wine 'c:\Python27\python.exe' -m PyInstaller --clean --onefile --upx-dir 'c:\wz_3373908\upx_3.94-W' 'c:\wz_3373908\src\parselab.py'

setup(
    name='wz_3373908',
    version='0.0.1',
    url='https://bitbucket.org/koi08r/wz_3373908',
    description="Parse list.xlsx and sort+copy files in work dir",
    package_dir={'': 'src'},
    packages=[],
    include_package_data=True,
    requires=[],
    setup_requires=[
        'PyInstaller'
    ],
    install_requires=[
        'xlrd'
    ],
    author='koi8-r',
    author_email='koi8-r@oz.net.ru',
    license='MIT',
    zip_safe=False,
    scripts=[],
    classifiers=[
        'Environment :: Console',
        'Programming Language :: Python :: 2.7'
    ]
)
