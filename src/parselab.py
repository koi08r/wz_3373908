# coding=utf-8
from __future__ import unicode_literals, print_function
from os import listdir, makedirs
from os.path import isdir, join, splitext
from shutil import copyfile
from functools import partial
from sys import version_info, exit
from six import text_type


try:
    from tkinter import Tk
    from tkinter import messagebox
    from tkinter.filedialog import askdirectory
except ImportError:
    raise RuntimeError('Отсутсвует библиотека Tk')


db_filename = 'list.xlsx'
Tk().withdraw()


class AppError(Exception):
    def __init__(self, caption='Ошибка', message='Произошла ошибка'):
        Exception.__init__(self)
        self.caption = caption
        self.message = message


try:
    if not (version_info.major, version_info.minor,) == (2, 7,):
        raise AppError(caption='Несовместимая версия python',
                       message='Для работы необходим python версии 2.7')
    try:
        import xlrd
    except ImportError:
        raise AppError(caption='Не установлен xlrd',
                       message='Для работы необходима python библиотека xlrd (python -m pip install xlrd)')

    work_dir = askdirectory(initialdir='../work/')
    if not work_dir:
        raise AppError(caption='Не выбрана рабочая директория',
                       message='Выберете рабочую директорию')

    try:
        wb = xlrd.open_workbook(join(work_dir, db_filename), on_demand=True)
    except IOError:
        raise AppError(message='Не найден {} в {}'.format(db_filename, work_dir))
    else:
        sheet = wb.sheet_by_index(0)  # A: фрагмент имени, B: Строка, C: Папка назначения
        row = 0
        db = dict()
        while True:
            try:
                row += 1
                row_values = tuple(text_type(sheet.cell_value(row, _)).strip() or None for _ in range(3))
                if None in row_values:
                    raise AppError(message='Пустое значение в строке {}'.format(row))
                else:
                    db[row_values[0]] = tuple(row_values[1:])  # (frag, string, folder) = row_values
                    # print('{} {} {}'.format(*row_values))
            except IndexError:
                break


    def evaluate(d, f):
        _f = f.upper()
        for _ in (k for k in (_.upper() for _ in d.keys()) if _f.startswith(k)):
            print('Found: {} <-> {}'.format(_, f))
            return d.get(_)
        else:
            print('Not found: {}'.format(f))
            return None

    def directory_visit(_dir, cb):
        for _ in listdir(_dir):
            f = join(_dir, _)
            if isdir(f):
                directory_visit(f, cb)
            else:
                fn, dst_dir = evaluate(db, _) or [None, None]
                if fn and dst_dir:
                    file_name_new = '{0}{1}{2}'.format(fn, *splitext(_))
                    cb(_dir, dst_dir, _, file_name_new)


    def callback(w_dir, s_dir, d_dir, file_name_old, file_name_new):
        dst_dir = join(w_dir, d_dir)
        try:
            makedirs(dst_dir)
        except OSError:
            pass

        s, d = join(s_dir, file_name_old), join(dst_dir, file_name_new)
        try:
            if join(s_dir, file_name_old) == join(dst_dir, file_name_old):
                print('Skip: {} -> {}'.format(s, d))
            else:
                print('Copy: {} -> {}'.format(s, d))
                copyfile(s, d)
        except (Exception, AppError) as e:
            if e.__class__ in (AppError, UnicodeDecodeError, UnicodeEncodeError):
                raise
            if e.__class__ is IOError:
                import errno
                if e.errno == errno.ENAMETOOLONG:
                    raise AppError(caption='Не могу скопировать файл',
                                   message='Слишком длинное имя файла {}'.format(s))
            raise AppError(caption='Не могу скопировать файл',
                           message='Ошибка при копировании {} -> {}'.format(s, d))


    directory_visit(work_dir, partial(callback, work_dir))
    messagebox.showinfo(title='Завершено', message='Обработаны файлы в {}'.format(work_dir))
except AppError as e:
    messagebox.showerror(e.caption, e.message)
    exit(255)
