from functools import partial


def evaluate(f, d):
    pass


_evaluate = partial(evaluate, dict())


def compose(*f):
    pass


compose(lambda: None, _evaluate)
